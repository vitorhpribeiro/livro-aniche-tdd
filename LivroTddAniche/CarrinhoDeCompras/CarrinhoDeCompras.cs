﻿using System.Collections.Generic;

namespace CarrinhoDeCompras
{
    public class CarrinhoDeCompras
    {
        internal IList<Item> Itens { get; set; }

        public CarrinhoDeCompras()
        {
            this.Itens = new List<Item>();
        }

        public void Adiciona (Item item)
        {
            this.Itens.Add(item);
        }
    }
}
