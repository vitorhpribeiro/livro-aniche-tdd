﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarrinhoDeCompras;

namespace CarrinhoDeCompras.Testes
{
    [TestFixture]
    class CarrinhoDeComprasTeste
    {
        [Test]
        public void Deve_retornar_zero_se_o_carrinho_esta_vazio()
        {
            CarrinhoDeCompras carrinhoDeCompras = new CarrinhoDeCompras();
            MaiorPreco maiorPrecoEsperado = new MaiorPreco();
            MaiorPreco valorASerPago = new MaiorPreco();
            var valorObtido = valorASerPago.EncontraMaiorPreco(carrinhoDeCompras);

            var valorEsperado = maiorPrecoEsperado.EncontraMaiorPreco(carrinhoDeCompras);

            Assert.AreEqual(valorEsperado, valorObtido, 0.00001);
        }

        [Test]
        public void Deve_retornar_o_preco_do_unico_item()
        {
            CarrinhoDeCompras carrinhoDeCompras = new CarrinhoDeCompras();
            carrinhoDeCompras.Adiciona(new Item("Geladeira", 1, 1.900));
            MaiorPreco algoritmo = new MaiorPreco();

            var valorObtido = algoritmo.EncontraMaiorPreco(carrinhoDeCompras);

            Assert.AreEqual(1.900, valorObtido, 0.00001);
        }
    }
}
