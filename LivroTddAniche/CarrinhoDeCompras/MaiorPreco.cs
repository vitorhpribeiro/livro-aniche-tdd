﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarrinhoDeCompras
{
    class MaiorPreco
    {
        public double EncontraMaiorPreco(CarrinhoDeCompras carrinhoDeCompras)
        {
            if (carrinhoDeCompras.Itens.Count == 0)
            {
                return 0.0;
            }
            return carrinhoDeCompras.Itens[0].valorTotal();
        }

    }
}
