﻿namespace CarrinhoDeCompras
{
    internal class Item
    {
        public string _descricao { get; private set; }
        public int _quantidade { get; private set; }
        public double _valorUnitario { get; private set; }

        public Item (string descricao, int quantidade, double valorUnitario)
        {
            this._descricao = descricao;
            this._quantidade = quantidade;
            this._valorUnitario = valorUnitario;
        }

        public double valorTotal()
        {
            return this._quantidade * this._valorUnitario;
        }
    }
}